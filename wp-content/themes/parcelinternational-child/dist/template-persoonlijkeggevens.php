<?php

/* Template Name: persoonlijkegegevens */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper dashboard" id="page-wrapper">

	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-lg-3">
				<?php dynamic_sidebar('mijnparcelinternational'); ?>
				<?php dynamic_sidebar('uwaccount'); ?>
			</div>

			<div class="col-lg-9">
				<?php get_template_part( 'partials/content', '' ); ?>
			</div>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->


<?php get_footer(); ?>