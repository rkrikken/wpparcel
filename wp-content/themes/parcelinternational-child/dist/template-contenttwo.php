<?php

/* Template Name: New Content PAge */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="content-new">

<section class="content__hero">

	<img src="<?php get_home_url(); ?>/wp-content/uploads/2018/10/hero-bg.png">

	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-sm-12">
				<div class="content__hero-content">
					<div class="content__hero-header">
						<h1 class="bold"><?php the_field('contenttitle'); ?></h1>
						<p class="sub"><?php the_field('contentsub'); ?></p>
						<div class="btn-outer">
							<a class="register btn-02" href=""><?php the_field('contentbtnname'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content__images">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row no-gutters padding">

					<?php

					// check if the repeater field has rows of data
					if( have_rows('repeaterimages') ):

					 	// loop through the rows of data
					    while ( have_rows('repeaterimages') ) : the_row(); 

						?>

					        <div class="col">
								<div class="content__images-content">
									<?php echo wp_get_attachment_image( get_sub_field('image'), '900w',"", ["class" => "content-image"] ); ?>
								</div>
							</div>

					   <?php 

						endwhile;

					else :

					    // no rows found

					endif;

					?>

				</div>
			</div>
		</div>
	</div>
</section>




<?php

// check if the flexible content field has rows of data
if( get_field('flexcontent') ):

 	// loop through the rows of data
    while ( has_sub_field('flexcontent') ) :

		


			// check current row layout
        if( get_row_layout() == 'usparea' ):

        	// check if the nested repeater field has rows of data
        	if( have_rows('usprepeater') ): ?>

				<section class="content__usp">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="content__usp-content">
									<div class="owl-carousel owl-theme" id="uspSlider">

				<?php

					// loop through the rows of data
				while ( have_rows('usprepeater') ) : the_row(); ?>

					<?php

					$usp = get_sub_field('uspitem');

					?>

					

						<div class="item">
							<div class="check"><i class="fa fa-check"></i></div>
							<h4><?php echo $usp; ?></h4>
						</div>				
										
					<?php 

				endwhile; ?>

									</div>
								</div>
							</div>
	        			</div>
	        		</div>
	        	</section>

				<?php endif;


        // check current row layout
        elseif( get_row_layout() == 'repeater' ):

        	// check if the nested repeater field has rows of data
        	if( have_rows('repeater') ): ?>

			 	
				<section class="content__page">
					<div class="container">
						<div class="row">

				<?php

					// loop through the rows of data
				while ( have_rows('repeater') ) : the_row(); ?>

					<div class="col-lg col-sm-12">
					<div class="content__page-content">

					<?php

					$title = get_sub_field('title');
					$sub = get_sub_field('sub');
					$link = get_sub_field('link');

					?>

					<h2 class="bold"><?php echo $title ?></h2>
					<p><?php echo $sub ?></p>

					<?php if ($link) { ?>

					<a class="btn-02" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

					<?php } ?>

					
					</div>
					</div>
										
					<?php 

				endwhile; ?>

						</div>
					</div>
				</section>

			<?php 

			

				endif;

			endif;

    endwhile;

else :

    // no layouts found

endif;

?>






</div><!-- Wrapper end -->

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">

			</main><!-- #main -->

		</div><!-- .row -->

	</div><!-- Container end -->



<?php get_footer(); ?>
