<?php

/* Template Name: Template Question */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<?php if(is_page_template( 'question.php' )) : ?>

	<style>
	.navbar-expand-xl{
		background-color: #ffffff!important;
	}

	.topnav__container {
		background-color: #ffffff!important;
	}
	</style>

<?php endif; ?>

<?php get_template_part( 'loop-templates/content', 'page-question' ); ?>

<?php get_footer(); ?>