<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>

<article class="offerteopmaat" id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<?php get_template_part('partials/content', 'introofferte'); ?>
		<?php get_template_part('partials/content', 'testimonial'); ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
