<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>

<article class="zendingsprofiel" id="post-<?php the_ID(); ?>">

	<div class="entry-content">

		<?php get_template_part('partials/content', 'zendingsprofiel'); ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
