<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<article class="single-storie" id="post-<?php the_ID(); ?>">
	<div class="row d-flex justify-content-center">

		<div class="col-lg-10">

			<div class="entry-header">

<!-- flexibele content -->

<?php

// check if the flexible content field has rows of data
if( have_rows('story_choose_content_header') ):

     // loop through the rows of data
    while ( have_rows('story_choose_content_header') ) : the_row();

        if( get_row_layout() == 'titel' ): ?>


			<progress value="0"></progress>

        	<div class="a-m center">

        		<?php 

        		$readtime = get_field('lees_tijd');

        		if ($readtime) { ?>

				<p class="read__time"><?php the_field('lees_tijd'); ?></p>

				<?php } ?>

        		<h1 class="entry-title"><?php the_sub_field('titel'); ?></h1>

        	<?php

        elseif( get_row_layout() == 'introductie' ): ?>

        		<p class="entry-sub center"><?php the_sub_field('introductie'); ?></p>

        	</div><!-- end a-m center -->

        	<?php

         elseif( get_row_layout() == 'paragraaf' ): ?>

			 <section class="paragraph">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<p><?php the_sub_field('paragraaf'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

			</div><!-- .entry-header -->

		</div>

	</div>

</article><!-- #post-## -->

<!-- end header section -->

</div> <!-- end row -->
</div>	<!-- end container --> 
</wrapper> <!-- end wrapper -->

<?php if( has_post_thumbnail()) { ?>

<section class="storie-content">

	<?php get_template_part('partials/content', 'devider'); ?>

	<div class="container">
		<div class="row d-flex ">
			<div class="col-lg-12">
				<div class="storie-introimg card">
					 <?php echo get_the_post_thumbnail( $post->ID, '1700' ); ?> 
				</div>
			</div>
		</div>
	</div>

</section>

<?php } else { ?>

<style>

#content {
	padding: 50px 0;
}

#storyWrapper {
	padding: 50px 0;
}
</style>

<?php } ?>

<div id="storyWrapper"> <!-- start story wrapper -->



<!-- flexibele content -->

<?php

// check if the flexible content field has rows of data
if( have_rows('story_choose_content_body') ):

     // loop through the rows of data
    while ( have_rows('story_choose_content_body') ) : the_row();

        if( get_row_layout() == 'paragraaf' ): ?>

			 <section class="paragraph">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<p><?php the_sub_field('paragraaf'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'quoteparagraaf' ): ?>

			<section class="quoteParagraph">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 d-flex justify-content-center">
							<div class="box">
								<h4 class="stories color"><?php the_sub_field('quote'); ?></h4>
							</div>
						</div>
						<div class="col-lg-12">
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'cta' ): ?>

			<section class="singlecta">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="singlecta__content">
								<div class="singlecta__title">
									<h3><?php the_sub_field('tekst'); ?></h3>
								</div>

								<?php 

								$link = get_sub_field('cta_link');
								$linktwo = get_sub_field('cta_pagelink');

								if ($link) { ?>

								<div class="singlecta__btn">
									<a class="btn-02" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo the_sub_field('cta_title'); ?></a>

								</div>

								<?php } ?>

								<?php if ($linktwo) { ?>

								<div class="singlecta__btn">
									<a class="btn-02" href="<?php echo $linktwo['url']; ?>" target="<?php echo $linktwo['target']; ?>"><?php echo the_sub_field('cta_title'); ?></a>
								</div>

								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'doubleimg' ): ?>

			<section class="doubleImg">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="img-box card">
								<?php echo wp_get_attachment_image( get_sub_field('imgone'), '1700w',"", ["class" => ""] ); ?>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="img-box card">
							 	<?php echo wp_get_attachment_image( get_sub_field('imgtwo'), '1700w',"", ["class" => ""] ); ?>
							 </div>
						</div>
						<div class="col-lg-12">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'paragraaftitel' ): ?>

			<section class="paragraphTitle">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2><?php the_sub_field('titel'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'singleimage' ): ?>

			<section class="singleImg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="img-box card">
								 <?php echo wp_get_attachment_image( get_sub_field('imgone'), '1700w',"", ["class" => ""] ); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'singlecta' ): 

        		 $pagelink = get_sub_field('pagelink'); 
        		 $link = get_sub_field('link'); 
        		 $title = get_sub_field('singlectatitle');
        		 $classname = get_sub_field('classname');

        		 if($pagelink) { ?>

				<section class="singlecta">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="singlecta-outer">
									<a class="singlectabtn <?php echo $classname; ?>" href="<?php echo $pagelink ?>" target="_blank"><?php echo $title ?></a>
								</div>
							</div>
						</div>
					</div>
				</section>

				<?php } else { ?>
					<section class="singlecta">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="singlecta-outer">
										<a class="singlectabtn <?php echo $classname; ?>" href="<?php echo $link ?>" target="_blank"><?php echo $title ?></a>
									</div>
								</div>
							</div>
						</div>
					</section>
				<?php }

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<?php if( the_tags() ){ ?>

<section class="tagcloud">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="tags">
					 <?php the_tags( '<ul><li class="tag">', '</li><li class="tag">', '</li></ul>' ); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php } ?>

</div> <!-- end story wrapper -->

<section class="postedBy">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-12">
				<div class="postedBy__head">
					<div class="author">
						<?php  
						    $user_id = 1;
						    echo get_avatar($user_id, 60); 
						?>
						<div class="author__description">
							<h4><?php the_field('Main_title', option); ?></h4>

							<p><?php the_field('Main_sub', option); ?></p>
						</div>
					</div>

					<div class="author__content">
						<p><?php the_field('Main_description', option); ?></p>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('partials/content', 'morecontent'); ?>

<div class="row"> <!-- start row -->