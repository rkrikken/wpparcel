<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<div class="col-lg-4 col-md-6 col-sm-12"> 

<article class="overview card" id="post-<?php the_ID(); ?>">

	<?php 

	$thumb = get_the_post_thumbnail( $post->ID, 'large' );

	if ($thumb) { ?>

		 <div class="img-outer">

			<?php echo $thumb; ?>

		</div>

	<?php } ?>

	<div class="entry-content">

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
		'</a></h2>' ); ?>

		<?php
		the_excerpt();
		?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->

</div>
