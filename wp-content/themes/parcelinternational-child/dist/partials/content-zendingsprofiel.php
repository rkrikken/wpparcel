		<?php

		// check if the flexible content field has rows of data
		if( have_rows('flex') ):

		     // loop through the rows of data
		    while ( have_rows('flex') ) : the_row();

		        if( get_row_layout() == 'intro' ): ?>

		        	<section class="introduction__campaign">
						<div class="hide">
							<?php get_template_part('partials/content', 'svg'); ?>
						</div>

			        	<div class="row d-flex justify-content-center">
							<div class="col-lg-6">
								<div class="introduction__content center fadeout">

							

					        <h1 id="titlelarge"> <?php the_sub_field('title'); ?> </h1>
					        <p>	<?php the_sub_field('sub'); ?> </p>


								</div>
							</div>
						</div>
					</section>

		         <?php elseif( get_row_layout() == 'cta_block' ): ?>

		        	
					<section class="campaign__contact" id="scrollone">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 d-flex justify-content-center">
									<div class="whoarewe">
										<h2><?php the_sub_field('titel'); ?></h2>
										<p>
										<?php the_sub_field('tekst'); ?>
										</p>

										<quote>
											<?php the_sub_field('quotetekst'); ?>
											<span>
												<div class="left">


													<?php 

													$image = get_sub_field('image');
													$size = 'full'; // (thumbnail, medium, large, full or custom size)

													if( $image ) {

														echo wp_get_attachment_image( $image, $size );

													}

													?>
													
												</div>
												<div class="right">
													<strong><?php the_sub_field('naam'); ?></strong> 
													</br> <?php the_sub_field('functie'); ?>
												</div>
											</span>
										</quote>
									</div>

								</div>
								<div class="col-lg-6">
									<h2><?php the_sub_field('contactveld_titel'); ?></h2>
									<div id="campaign__form">
									<?php the_sub_field('contactveld'); ?>
									</div>
								</div>
					</section>

		        <?php endif;

		    endwhile;

		else :

		    // no layouts found

		endif;

		?>
