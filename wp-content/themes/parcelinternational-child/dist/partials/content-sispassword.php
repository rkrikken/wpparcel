<section class="sislogin">

	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="container d-flex justify-content-center">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-8">
				<div class="login">
					<div class="login__header">
						<div class="login__header-content">
							<h1 class="dark"><?php the_field('password_title'); ?></h1>
							<p class="sub dark"><?php the_field('password_sub'); ?></p>
						</div>
					</div>

					<div class="login__body">
						<div class="login__body-content">

							<?php the_content(); ?>

						</div>
					</div>

					<div class="login__footer">

					</div>

				</div>

			</div>
		</div>
	</div>

</section>
