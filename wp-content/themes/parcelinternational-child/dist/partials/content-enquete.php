<section class="side-widget">
	<div class="side-widget-toggle">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="enquete__content">
					<div class="enquete__content-header">
						<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?></a>

					</div>
					<div class="enquete__content-body">
						<?php echo do_shortcode('[contact-form-7 id="1488" title="survey"]'); ?>
					</div>
					<div class="enquete__content-footer">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>