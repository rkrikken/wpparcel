<section class="sislogin">

	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="container d-flex justify-content-center">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-8">
				<div class="login">
					<div class="login__header">
						<div class="login__header-content">
							<h1 class="dark"><?php the_field('login_title'); ?></h1>
							<p class="sub dark"><?php the_field('login_subtitle'); ?></p>
						</div>
					</div>

					<div class="login__body">
						<div class="login__body-content">

							<form action="<?php echo the_field('redirect', option); ?>" method="post" class="form" target="_blank">
								<label><?php the_field('username'); ?></label>
								<input type="text" name="userid" placeholder="<?php the_field('username'); ?>">


							 	<label><?php the_field('password'); ?></label>
								<input type="password" name="password" placeholder="<?php the_field('password'); ?>">
							   
							    <div class="checkbox">
									<input required="required" type="checkbox" name="confirm" value="OK"><?php the_field('login_agreement'); ?></a>
								</div>

							    <button class="btn-sislogin" type="submit"><?php the_field('button_title'); ?></button>
								
							</form>

							<a class="sis-link" href="<?php echo the_field(password_pagelink); ?>"><em><?php the_field('forget_password'); ?></em></a>

						</div>
					</div>

					<div class="login__footer">

					</div>

				</div>

			</div>
		</div>
	</div>

</section>
