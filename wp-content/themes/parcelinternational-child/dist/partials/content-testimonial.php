<section class="testimonial d-flex">
	<div class="testimonial__introduction center">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-8">
					<h2><?php the_field('testimonial_title'); ?></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="owl-carousel owl-theme" id="testimonialSlider">

			<?php if( have_rows('testimonial_slider') ): ?>

				<?php while( have_rows('testimonial_slider') ): the_row(); 

					// vars
				
					$testimonial = get_sub_field('testimonial');
					$quote = get_sub_field('testimonial_quote');
					$name = get_sub_field('testimonial_name');
					$company = get_sub_field('testimonial_company');

					?>
							
								<div class="item">
									<div class="container">
										<div class="testimonial__card card">
											<div class="col-lg-12 d-flex align-items-stretch">
												<div class="row">
													<div class="col-lg-8 d-flex align-items-stretch no-gutters">
														<div class="testimonial__content">
															<p>
															<?php echo $testimonial ?>
															</p>
															<div class="testimonial__logo">
																<?php echo wp_get_attachment_image( get_sub_field('testimonial_logo'), '400w',"", ["class" => "person"] ); ?>
															</div>
														</div>
													</div>
													<div class="col-lg-4 d-flex align-items-stretch flex-column no-gutters">
														<div class="testimonial__quote">
															<p>
															<?php echo $quote ?>
															</p>
															<h3 class="testimonial__title">
																<?php echo $name ?>
															</h3>
															<a class="testimonial__link">
																<?php echo $company ?>
															</a>
														</div>
														<!-- <div class="testimonial__footer">
															<div class="testimonial__description">
																
															</div>
														</div> -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

						<?php endwhile; ?>	

				<?php endif; ?>
				
			</div>
		</div>
	</div>
</section>