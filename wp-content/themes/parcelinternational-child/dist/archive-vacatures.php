<?php

get_header();
?>

<?php
$container   = get_theme_mod( 'understrap_container_type' );
?>

<section class="archiveintro">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-10">
				<div class="archiveintro-content">
					<h1><?php the_field('title_careers', options);?></h1>
					<p><?php the_field('text_careers', options); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="wrapper" id="archive-wrapper">

	<div class="container" id="content-vacatureoverview" tabindex="-1">

		<div class="row">

					<?php if ( have_posts() ) : ?>

						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'loop-templates/content', 'get_post_format()' );
							?>

						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

	</div> <!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
