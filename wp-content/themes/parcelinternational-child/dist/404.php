<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

<section class="question">
	
	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-8">
				<div class="question__body">
					<div class="question__body-content">
					
					<div class="page-wrapper">
						<div class="page-content">
							
								<h1 class="page-title">404 - Not Found</h1>
							
							<h4><?php _e( 'This is somewhat embarrassing, isn’t it?', '' ); ?></h4>
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', '' ); ?></p>

							<?php get_search_form(); ?>
						</div><!-- .page-content -->
					</div><!-- .page-wrapper -->

					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_footer(); ?>