<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

		<section class="footermenus">
			<div class="container">
				<div class="footermenus__intro">
					<div class="footermenus__intro-title">
						<h4 class="light"><?php the_field('footer_cta_large', option); ?></h4>
					</div>
					<div class="footermenus__intro-cta">
						<a class="btn-02" id="register-footer" href="<?php echo the_field('footer_cta_pagelink', option) ?>"><?php the_field('footer_cta_title', option); ?></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
						<div class="col-lg-3 col-sm-12">
							<div class="footermenus__outer">
							<div class="footermenus__outer-inner">
								<span class="plus-one icon"><i class="fa fa-angle-down"></i></span>
								<?php dynamic_sidebar('footerone'); ?>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-12">
							<div class="footermenus__outer">
							<div class="footermenus__outer-inner">
								<span class="plus-two icon"><i class="fa fa-angle-down"></i></span>
								<?php dynamic_sidebar('footertwo'); ?>
							</div>
							</div>
						</div>
		</div><!-- extra closing div -->
						<div class="col-lg-3 col-sm-12">
							<div class="footermenus__outer">
							<div class="footermenus__outer-inner">
								<span class="plus-three icon"><i class="fa fa-angle-down"></i></span>
								<?php dynamic_sidebar('footerthree'); ?>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-sm-12">
							<div class="footermenus__outer">
							<div class="footermenus__outer-inner">
								<span class="plus-four icon"><i class="fa fa-angle-down"></i></span>
								<?php dynamic_sidebar('footerfour'); ?>
							</div>
							</div>
						</div>
					</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="afterfooter">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="afterfooter__content">
							<div class="afterfooter__content-logo">

								<?php 

									$image = get_field('footer_logo', 'option');

									if( !empty($image) ): ?>

										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

								<?php endif; ?>

							</div>
							<div class="afterfooter__content-items">
								<?php dynamic_sidebar('footerwidget-one'); ?>
								<p class="copyright"><?php the_field('footer_year', option);?></p>
							</div>
							<div class="afterfooter__content-items-two">
								<!-- <?php dynamic_sidebar('footerwidget-two'); ?> -->
								<p><?php the_field('footer_content', option);?></p>
								<p class="company__info"><?php the_field('footer_company_info', option);?></p>

								<?php dynamic_sidebar('footersix'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	

</div><!-- wrapper end -->

</div> <!-- end page content rapper -->

</div><!-- /#wrapper -->

</div><!-- #page we need this extra closing tag here -->

</div><!-- /#page-content-wrapper -->

<!-- START FRESHDESK -->

<script type="text/javascript">var fc_CSS=document.createElement('link');fc_CSS.setAttribute('rel','stylesheet');var isSecured = (window.location && window.location.protocol == 'https:');var rtlSuffix = ((document.getElementsByTagName('html')[0].getAttribute('lang')) === 'ar') ? '-rtl' : '';fc_CSS.setAttribute('type','text/css');fc_CSS.setAttribute('href',((isSecured)? 'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets1.chat.freshdesk.com')+'/css/visitor'+rtlSuffix+'.css');document.getElementsByTagName('head')[0].appendChild(fc_CSS);var fc_JS=document.createElement('script'); fc_JS.type='text/javascript';fc_JS.src=((isSecured)?'https://d36mpcpuzc4ztk.cloudfront.net':'http://assets.chat.freshdesk.com')+'/js/visitor.js';(document.body?document.body:document.getElementsByTagName('head')[0]).appendChild(fc_JS);window.freshchat_setting= 'eyJ3aWRnZXRfc2l0ZV91cmwiOiJwYXJjZWxpbnRlcm5hdGlvbmFsLmZyZXNoZGVzay5jb20iLCJwcm9kdWN0X2lkIjpudWxsLCJuYW1lIjoiUGFyY2VsaW50ZXJuYXRpb25hbCIsIndpZGdldF9leHRlcm5hbF9pZCI6bnVsbCwid2lkZ2V0X2lkIjoiOGU4MmYyYjktODhlOS00Y2VjLTgxOWYtYzU1ZTZiNGZjN2EwIiwic2hvd19vbl9wb3J0YWwiOnRydWUsInBvcnRhbF9sb2dpbl9yZXF1aXJlZCI6ZmFsc2UsImlkIjo0MDAwMDAyNTY3LCJtYWluX3dpZGdldCI6dHJ1ZSwiZmNfaWQiOiJkYmI1NTc1M2ZhOGZlMDEwZDlkOGNiYWRmOGQ1ZjZhZCIsInNob3ciOjEsInJlcXVpcmVkIjoyLCJoZWxwZGVza25hbWUiOiJQYXJjZWxpbnRlcm5hdGlvbmFsIiwibmFtZV9sYWJlbCI6Ik5hYW0iLCJtYWlsX2xhYmVsIjoiRS1tYWlsYWRyZXMiLCJtZXNzYWdlX2xhYmVsIjoiTWVzc2FnZSIsInBob25lX2xhYmVsIjoiVGVsZWZvb25udW1tZXIiLCJ0ZXh0ZmllbGRfbGFiZWwiOiJUZWtzdHZlbGQiLCJkcm9wZG93bl9sYWJlbCI6IkRyb3Bkb3duIiwid2VidXJsIjoicGFyY2VsaW50ZXJuYXRpb25hbC5mcmVzaGRlc2suY29tIiwibm9kZXVybCI6ImNoYXQuZnJlc2hkZXNrLmNvbSIsImRlYnVnIjoxLCJtZSI6IklrIiwiZXhwaXJ5IjowLCJlbnZpcm9ubWVudCI6InByb2R1Y3Rpb24iLCJkZWZhdWx0X3dpbmRvd19vZmZzZXQiOjMwLCJkZWZhdWx0X21heGltaXplZF90aXRsZSI6IkJlemlnIG1ldCBjaGF0dGVuIiwiZGVmYXVsdF9taW5pbWl6ZWRfdGl0bGUiOiJadWxsZW4gd2UgZ2FhbiBjaGF0dGVuPyIsImRlZmF1bHRfdGV4dF9wbGFjZSI6IlV3IGJlcmljaHQiLCJkZWZhdWx0X2Nvbm5lY3RpbmdfbXNnIjoiV2FjaHRlbiBvcCBlZW4gbWVkZXdlcmtlciIsImRlZmF1bHRfd2VsY29tZV9tZXNzYWdlIjoiSGFsbG8hIFdhdCBrdW5uZW4gd2Ugdm9vciB1IGRvZW4/IiwiZGVmYXVsdF93YWl0X21lc3NhZ2UiOiJFZW4gdmFuIG9uemUgbWVkZXdlcmtlcnMga29tdCB6byBiaWogdS4gRWVuIG9nZW5ibGlrIGdlZHVsZC4iLCJkZWZhdWx0X2FnZW50X2pvaW5lZF9tc2ciOiJ7e2FnZW50X25hbWV9fSBuZWVtdCBvb2sgZGVlbCBhYW4gZGUgY2hhdC4iLCJkZWZhdWx0X2FnZW50X2xlZnRfbXNnIjoie3thZ2VudF9uYW1lfX0gaGVlZnQgZGUgY2hhdCB2ZXJsYXRlbi4iLCJkZWZhdWx0X2FnZW50X3RyYW5zZmVyX21zZ190b192aXNpdG9yIjoiWW91ciBjaGF0IGhhcyBiZWVuIHRyYW5zZmVycmVkIHRvIHt7YWdlbnRfbmFtZX19IiwiZGVmYXVsdF90aGFua19tZXNzYWdlIjoiQmVkYW5rdCB2b29yIGplIGNoYXQuIEFscyBqZSBub2cgdnJhZ2VuIGhlYnQsIGt1biBqZSBvbnMgYWx0aWpkIGV2ZW4gcGluZ2VuLiIsImRlZmF1bHRfbm9uX2F2YWlsYWJpbGl0eV9tZXNzYWdlIjoiSGVsYWFzIHppam4gYWwgb256ZSBtZWRld2Vya2VycyBpbiBnZXNwcmVrLiBBbHMgdSBlZW4gYmVyaWNodGplIGFjaHRlcmxhYXQsIG5lbWVuIHdlIHpvIHNuZWwgbW9nZWxpamsgY29udGFjdCBtZXQgdSBvcC4iLCJkZWZhdWx0X3ByZWNoYXRfbWVzc2FnZSI6IldlIGt1bm5lbiBvcCBkaXQgbW9tZW50IG5pZXQgbWV0IHUgY2hhdHRlbi4gTWlzc2NoaWVuIHdpbHQgdSBlZXJzdCBldmVuIGRlIHRpamQgbmVtZW4gb20gaWV0cyBtZWVyIG92ZXIgdXplbGYgdGUgdmVydGVsbGVuPyIsImFnZW50X3RyYW5zZmVyZWRfbXNnIjoiWW91ciBjaGF0IGhhcyBiZWVuIHRyYW5zZmVycmVkIHRvIHt7YWdlbnRfbmFtZX19IiwiYWdlbnRfcmVvcGVuX2NoYXRfbXNnIjoie3thZ2VudF9uYW1lfX0gcmVvcGVuZWQgdGhlIGNoYXQiLCJ2aXNpdG9yX3NpZGVfaW5hY3RpdmVfbXNnIjoiVGhpcyBjaGF0IGhhcyBiZWVuIGluYWN0aXZlIGZvciB0aGUgcGFzdCAyMCBtaW51dGVzLiIsImFnZW50X2Rpc2Nvbm5lY3RfbXNnIjoie3thZ2VudF9uYW1lfX0gaGFzIGJlZW4gZGlzY29ubmVjdGVkIiwic2l0ZV9pZCI6ImRiYjU1NzUzZmE4ZmUwMTBkOWQ4Y2JhZGY4ZDVmNmFkIiwiYWN0aXZlIjp0cnVlLCJ3aWRnZXRfcHJlZmVyZW5jZXMiOnsid2luZG93X2NvbG9yIjoiIzE4OTVjOCIsIndpbmRvd19wb3NpdGlvbiI6IkJvdHRvbSBSaWdodCIsIndpbmRvd19vZmZzZXQiOiI1IiwidGV4dF9wbGFjZSI6IlV3IGJlcmljaHQiLCJjb25uZWN0aW5nX21zZyI6IldhY2h0ZW4gb3AgZWVuIG1lZGV3ZXJrZXIiLCJhZ2VudF9sZWZ0X21zZyI6Int7YWdlbnRfbmFtZX19IGhlZWZ0IGRlIGNoYXQgdmVybGF0ZW4iLCJhZ2VudF9qb2luZWRfbXNnIjoie3thZ2VudF9uYW1lfX0gaXMgdG9lZ2V2b2VnZCIsIm1pbmltaXplZF90aXRsZSI6IkNoYXQgUGFyY2VsIEludGVybmF0aW9uYWwiLCJtYXhpbWl6ZWRfdGl0bGUiOiJDaGF0IFBhcmNlbCBJbnRlcm5hdGlvbmFsIiwid2VsY29tZV9tZXNzYWdlIjoiR29lZGVuZGFnLCB3YWFybWVlIGt1bm5lbiB3aWogdSB2YW4gZGllbnN0IHppam4/IiwidGhhbmtfbWVzc2FnZSI6IkJlZGFua3Qgdm9vciB1dyBjb250YWN0LCB3YW5uZWVyIHUgbm9nIHZyYWdlbiBoZWVmdCB2ZXJuZW1lbiB3aWogaGV0IGdyYWFnLiBEaXQga2FuIHZpYSBjaGF0LCBoZWxwQHBhcmNlbGludGVybmF0aW9uYWwubmwgb2YgdmlhIDAxMC0gNDE4IDEwMDAiLCJ3YWl0X21lc3NhZ2UiOiJFZW4gb2dlbmJsaWsgZ2VkdWxkIGFsc3R1YmxpZWZ0LCB3aWogcHJvYmVyZW4gdSB6byBzcG9lZGlnIG1vZ2VsaWprIHRlIGhlbHBlbi4ifSwicm91dGluZyI6eyJkcm9wZG93bl9iYXNlZCI6ImZhbHNlIiwiY2hvaWNlcyI6eyJNYW4iOlsiMCJdLCJGZW1hbGUiOlsiMCJdLCJkZWZhdWx0IjpbIjAiXX19LCJwcmVjaGF0X2Zvcm0iOnRydWUsInByZWNoYXRfbWVzc2FnZSI6IldhYXJtZWUga3VubmVuIHdpaiB1IHZhbiBkaWVuc3Qgemlqbj8iLCJwcmVjaGF0X2ZpZWxkcyI6eyJuYW1lIjp7InRpdGxlIjoiTmFtZSIsInNob3ciOiIyIn0sInBob25lIjp7InRpdGxlIjoiUGhvbmUiLCJzaG93IjoiMSJ9LCJkcm9wZG93biI6eyJ0aXRsZSI6IkRyb3BEb3duIiwib3B0aW9ucyI6WyJNYW4iLCJGZW1hbGUiXX0sImVtYWlsIjp7InRpdGxlIjoiRW1haWwiLCJzaG93IjoiMSJ9LCJ0ZXh0ZmllbGQiOnsidGl0bGUiOiJNZXNzYWdlIiwic2hvdyI6IjEifX0sImJ1c2luZXNzX2NhbGVuZGFyIjpudWxsLCJub25fYXZhaWxhYmlsaXR5X21lc3NhZ2UiOnsidGV4dCI6IkxhYXQgZWVuIGJlcmljaHQgYWNodGVyIiwiY3VzdG9tTGluayI6IjAiLCJjdXN0b21MaW5rVXJsIjoiIn0sInByb2FjdGl2ZV9jaGF0Ijp0cnVlLCJwcm9hY3RpdmVfdGltZSI6MTgwLCJzaXRlX3VybCI6InBhcmNlbGludGVybmF0aW9uYWwuZnJlc2hkZXNrLmNvbSIsImV4dGVybmFsX2lkIjpudWxsLCJkZWxldGVkIjpmYWxzZSwib2ZmbGluZV9jaGF0Ijp7InNob3ciOiIxIiwiZm9ybSI6eyJuYW1lIjoiTmFtZSIsImVtYWlsIjoiRW1haWwiLCJtZXNzYWdlIjoiTWVzc2FnZSJ9LCJtZXNzYWdlcyI6eyJ0aXRsZSI6IldpaiBuZW1lbiB6byBzcG9lZGlnIG1vZ2VsaWprIGNvbnRhY3Qgb3AuIiwidGhhbmsiOiJCZWRhbmt0IHZvb3IgdXcgYmVyaWNodCwgd2lqIG5lbWVuIHpvIHNwb2VkaWcgbW9nZWxpamsgY29udGFjdCBtZXQgdSBvcC4iLCJ0aGFua19oZWFkZXIiOiJCZWRhbmt0ISJ9fSwibW9iaWxlIjp0cnVlLCJjcmVhdGVkX2F0IjoiMjAxNC0wOS0xMVQwNzoxMDoxOS4wMDBaIiwidXBkYXRlZF9hdCI6IjIwMTUtMDUtMDRUMDU6NTc6NDIuMDAwWiJ9';</script>

<!-- END FRESHDESK -->

<!-- START TRACK & TRACE MODAL -->

<div id="tracktraceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tracktraceModalTitle" style="display: none;">
	<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
		    <div class="modal-header">
		    	<h4 class="modal-title" id="tracktraceModalTitle">Track &amp; Trace</h4>
		    </div>
		    <div class="modal-body">
			    <form action="" method="post" id="trackformid" class="form-inline trackcheck" data-unknown="Onbekende Track &amp; Trace code." "="" data-error="Er trad een onbekende fout op." style="position: relative;">
				    <div class="form-group">
					    <select name="carrier" class="form-control inverse">
						    <option selected="selected" value=""></option>
						    <option value="DHL">DHL</option>
						    <option value="DPD">DPD</option>
						    <option value="FED">Fedex</option>
						    <option value="TNT">TNT</option>
						    <option value="UPS">UPS</option>
					    </select>			
				    </div>
				    <div class="form-group">
					    <input class="form-control inverse" name="tracktracecode" value="" placeholder="Voer uw Track &amp; Trace code in" style="margin: 0 10px;">
				    </div>
				    <div class="form-group">
					    <button type="submit" class="btn btn-primary">
					    	<img src="/wp-content/uploads/2018/10/button-search@2x-1.png" alt="" width="22" height="22">
					    </button>
				    </div>
			    </form>
		    </div>
		</div>
	</div>
</div>

<!-- END TRACK & TRACE MODAL -->
<!-- START TRACK RESULT MODAL -->

<div id="trackResultModal" class="modal fade in">
      <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="far fa-times-circle"></i></span></button>
		      <h4 class="modal-title">Track &amp; Trace</h4>
		    </div>
		    <div class="modal-body">
			    <div id="map-canvas" style="display: block; width: 100%; height: 300px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(https://maps.gstatic.com/mapfiles/openhand_8_8.cur), default;"><div style="z-index: 1; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -173, -21);"><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 0px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -256px; top: 0px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -256px; top: -256px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 0px; top: -256px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: -256px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 0px;"></div></div><div style="position: absolute; z-index: 0; transform: matrix(8, 0, 0, 8, -1197, -789);"></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -173, -21);"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: -256px;"></div></div></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: -156px; top: -151px; z-index: -108;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: -147px; top: -146px; z-index: -103;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: 128px; top: 43px; z-index: 86;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: 80px; top: 46px; z-index: 89;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: 98px; top: 37px; z-index: 80;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"></div><div style="position: absolute; left: -142px; top: -136px; z-index: -108;"><div style="height: 100px; margin-top: -50px; margin-left: -50%; display: table; border-spacing: 0px;"><div style="display: table-cell; vertical-align: middle; white-space: nowrap; text-align: center;"><div style="color: rgb(0, 0, 0); font-size: 14px; font-family: Roboto, Arial, sans-serif;">A</div></div></div></div><div style="position: absolute; left: -133px; top: -131px; z-index: -103;"><div style="height: 100px; margin-top: -50px; margin-left: -50%; display: table; border-spacing: 0px;"><div style="display: table-cell; vertical-align: middle; white-space: nowrap; text-align: center;"><div style="color: rgb(0, 0, 0); font-size: 14px; font-family: Roboto, Arial, sans-serif;">B</div></div></div></div><div style="position: absolute; left: 142px; top: 58px; z-index: 86;"><div style="height: 100px; margin-top: -50px; margin-left: -50%; display: table; border-spacing: 0px;"><div style="display: table-cell; vertical-align: middle; white-space: nowrap; text-align: center;"><div style="color: rgb(0, 0, 0); font-size: 14px; font-family: Roboto, Arial, sans-serif;">C</div></div></div></div><div style="position: absolute; left: 94px; top: 61px; z-index: 89;"><div style="height: 100px; margin-top: -50px; margin-left: -50%; display: table; border-spacing: 0px;"><div style="display: table-cell; vertical-align: middle; white-space: nowrap; text-align: center;"><div style="color: rgb(0, 0, 0); font-size: 14px; font-family: Roboto, Arial, sans-serif;">D</div></div></div></div><div style="position: absolute; left: 112px; top: 52px; z-index: 80;"><div style="height: 100px; margin-top: -50px; margin-left: -50%; display: table; border-spacing: 0px;"><div style="display: table-cell; vertical-align: middle; white-space: nowrap; text-align: center;"><div style="color: rgb(0, 0, 0); font-size: 14px; font-family: Roboto, Arial, sans-serif;">E</div></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -173, -21);"><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i4!3i3!4i256!2m3!1e0!2sm!3i415114044!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=121337" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i3!3i3!4i256!2m3!1e0!2sm!3i415114056!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=122113" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i3!3i2!4i256!2m3!1e0!2sm!3i415114056!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=41595" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i4!3i2!4i256!2m3!1e0!2sm!3i415114044!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=40819" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i5!3i2!4i256!2m3!1e0!2sm!3i415114368!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=99720" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i3!2i5!3i3!4i256!2m3!1e0!2sm!3i415114368!3m9!2snl-NL!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2!23i1301875&amp;token=49167" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; transition-duration: 0ms; -webkit-transition-duration: 0ms; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"><div style="z-index: 1; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"></div><div style="z-index: 4; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: -156px; top: -151px; z-index: -108;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" usemap="#gmimap5" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"><map name="gmimap5" id="gmimap5"><area href="javascript:void(0)" log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="" style="cursor: pointer;"></map></div><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: -147px; top: -146px; z-index: -103;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" usemap="#gmimap6" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"><map name="gmimap6" id="gmimap6"><area href="javascript:void(0)" log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="" style="cursor: pointer;"></map></div><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: 128px; top: 43px; z-index: 86;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" usemap="#gmimap7" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"><map name="gmimap7" id="gmimap7"><area href="javascript:void(0)" log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="" style="cursor: pointer;"></map></div><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: 80px; top: 46px; z-index: 89;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" usemap="#gmimap8" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"><map name="gmimap8" id="gmimap8"><area href="javascript:void(0)" log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="" style="cursor: pointer;"></map></div><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: 98px; top: 37px; z-index: 80;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2_hdpi.png" draggable="false" usemap="#gmimap9" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; opacity: 1;"><map name="gmimap9" id="gmimap9"><area href="javascript:void(0)" log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="" style="cursor: pointer;"></map></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe frameborder="0" src="about:blank" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=38.095522,30.370253&amp;z=3&amp;t=m&amp;hl=nl-NL&amp;gl=US&amp;mapclient=apiv3" title="Klik om dit gebied in Google Maps te bekijken" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google4_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); -webkit-box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 134px; top: 60px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Kaartgegevens</div><div style="font-size: 13px;">Kaartgegevens ©2018 Google, INEGI</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 109px; bottom: 0px; width: 175px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="text-decoration: none; cursor: pointer; display: none;">Kaartgegevens</a><span>Kaartgegevens ©2018 Google, INEGI</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Kaartgegevens ©2018 Google, INEGI</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/nl-NL_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Gebruiksvoorwaarden</a></div></div><button draggable="false" title="Weergave op volledig scherm in- of uitschakelen" aria-label="Weergave op volledig scherm in- of uitschakelen" type="button" style="background-image: none; border: 0px; margin: 10px 14px; padding: 0px; position: absolute; cursor: pointer; -webkit-user-select: none; width: 25px; height: 25px; overflow: hidden; top: 0px; right: 0px; background-position: initial initial; background-repeat: initial initial;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></button><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" rel="noopener" title="Fouten in de wegenkaart of beelden melden aan Google" href="https://www.google.com/maps/@38.0955225,30.3702526,3z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Een kaartfout rapporteren</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;"><button draggable="false" title="Inzoomen" aria-label="Inzoomen" type="button" style="background-image: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; width: 28px; height: 27px; top: 0px; left: 0px; background-position: initial initial; background-repeat: initial initial;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><button draggable="false" title="Uitzoomen" aria-label="Uitzoomen" type="button" style="background-image: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; -webkit-user-select: none; width: 28px; height: 27px; top: 0px; left: 0px; background-position: initial initial; background-repeat: initial initial;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl_hdpi.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; width: 28px; height: 28px; cursor: url(https://maps.gstatic.com/mapfiles/openhand_8_8.cur), default; position: absolute; left: 0px; top: 0px;"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Pegman-besturingselement in Street View" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5_hdpi.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman beweegt over de kaart" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5_hdpi.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman-besturingselement in Street View" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/cb_scout5_hdpi.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Kaart 90 graden draaien" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; cursor: pointer; display: none;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4_hdpi.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-top-left-radius: 2px; border-top-right-radius: 2px; border-bottom-right-radius: 2px; border-bottom-left-radius: 2px; top: 0px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4_hdpi.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Stratenkaart weergeven" aria-label="Stratenkaart weergeven" aria-pressed="true" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 27px; font-weight: 500;">Kaart</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 29px; text-align: left; display: none;"><div draggable="false" title="Stratenkaart met terrein weergeven" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img alt="" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrein</label></div></div></div><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Satellietbeelden weergeven" aria-label="Satellietbeelden weergeven" aria-pressed="false" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 40px; border-left-width: 0px;">Satelliet</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 29px; text-align: left; display: none;"><div draggable="false" title="Beelden met straatnamen weergeven" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img alt="" src="https://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div></div>
			    <div class="table-responsive">
				    <table class="table table-striped" width="100%">
					    <tbody>
						    <tr><th>Carrier</th><td data-id="carrier">UPS</td></tr>
						    <tr><th>Official link:</th><td data-id="link">http://www.ups.com</td></tr>
						    <tr><th>Tracking nr</th><td data-id="awb">1Z5V842V0493974390</td></tr>
						    <tr><th>Status</th><td data-id="status">DONE</td></tr>
					    </tbody>
				    </table>
			    </div>
			    <p>History:</p>
			    <div class="table-responsive">
				    <table class="table table-striped" width="100%">
					    <tbody data-id="history"><tr><th>2018-03-09 08:58:00</th><td>BILLING INFORMATION RECEIVED</td><td class="nowrap">NL</td></tr><tr><th>2018-03-12 17:00:00</th><td>PICKUP SCAN</td><td class="nowrap">Eindhoven, NL</td></tr><tr><th>2018-03-12 20:09:00</th><td>ORIGIN SCAN</td><td class="nowrap">Eindhoven, NL</td></tr><tr><th>2018-03-12 22:30:00</th><td>DEPARTURE SCAN</td><td class="nowrap">Eindhoven, NL</td></tr><tr><th>2018-03-13 00:58:00</th><td>ARRIVAL SCAN</td><td class="nowrap">Koeln (cologne), DE</td></tr><tr><th>2018-03-13 04:11:00</th><td>DEPARTURE SCAN</td><td class="nowrap">Koeln (cologne), DE</td></tr><tr><th>2018-03-13 13:26:00</th><td>ARRIVAL SCAN</td><td class="nowrap">Dubai, AE</td></tr><tr><th>2018-03-13 20:30:00</th><td>DEPARTURE SCAN</td><td class="nowrap">Dubai, AE</td></tr><tr><th>2018-03-13 21:40:00</th><td>ARRIVAL SCAN</td><td class="nowrap">Riyadh, SA</td></tr><tr><th>2018-03-14 04:00:00</th><td>DEPARTURE SCAN</td><td class="nowrap">Riyadh, SA</td></tr><tr><th>2018-03-14 05:00:00</th><td>ARRIVAL SCAN</td><td class="nowrap">Dharan, SA</td></tr><tr><th>2018-03-14 08:41:00</th><td>OUT FOR DELIVERY</td><td class="nowrap">Dharan, SA</td></tr><tr><th>2018-03-14 09:35:00</th><td>DELIVERED</td><td class="nowrap">Dharan, SA</td></tr></tbody>
				    </table>
			    </div>
		    </div>
	    </div>
    </div>
  </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALjhhoq6TtpgPt85rOOEFMDJwdrJSlT0M&amp;sensor=false" defer></script>

	    <script>

		    var Parcel = {
	            map: null,
			    bounds: null,
			    infowindow: null,
			    markers: [],
			    labels: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			    labelIndex: 0,
			
			    initialize: function(status,history) {
				    if(Parcel.map == null) {
					    var properties = {
						    mapTypeId:google.maps.MapTypeId.ROADMAP
					    };
					    Parcel.map=new google.maps.Map(document.getElementById("map-canvas"), properties);
				    }
				
				    Parcel.bounds = new google.maps.LatLngBounds();
				
				    if (Parcel.infowindow) {
					    Parcel.infowindow.close();
				    }
				
				    if (Parcel.markers.length > 0) {
					    for (var i = 0; i < Parcel.markers.length; i++) {
						    Parcel.markers[i].setMap(null);
					    }
					    Parcel.labelIndex = 0;
				    }
				
				    if(history && history.length > 0) {
					    $(history).each(function(i,o) {
						    if (o.location && o.location.geo) {
							    historyLatLng = new google.maps.LatLng(o.location.geo.lat,o.location.geo.lng);
							    Parcel.bounds.extend(historyLatLng);
							    Parcel.markers.push(
								    new google.maps.Marker({
									    position: historyLatLng,
									    map: Parcel.map,
									    label: Parcel.labels[Parcel.labelIndex++ % Parcel.labels.length],
								    })
							    );
						    }
					    })
				    }
				
				    Parcel.map.fitBounds(Parcel.bounds);
				
				    var listener = google.maps.event.addListener(Parcel.map, "idle", function () {
					    Parcel.map.fitBounds(Parcel.bounds);
					    Parcel.map.setCenter(Parcel.bounds.getCenter());
					    google.maps.event.removeListener(listener);
				    });
				
				    google.maps.event.trigger(Parcel.map, "resize");
				    window.setTimeout(function() {
					    google.maps.event.trigger(Parcel.map, "resize");
				    },200);
				
			    }
	        }

	        jQuery(function ($) {

	        $("body").on('submit','#trackformid',function(e) {
		        e.preventDefault();
			    form = $(this);
			    code = form.find('input[name="tracktracecode"]').eq(0).val();
			    carrier = form.find('select[name="carrier"]').eq(0).val();
			    if (code && carrier) {
			        form.find('button').addClass('loading').prepend('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
				    $.ajax({
				        url: "http://i-ship-to.com/trackandtrace/"+carrier+"/"+code+"/json?callback=?",
					    dataType: 'jsonp',
					    jsonp: 'callback',
					    jsonpCallback: 'jsonpCallback',
					    type: 'GET',
					    success: function( data ) {
					        if(data != null && typeof(data['info']) != 'undefined' && typeof(data['info']['carrier']) != 'undefined' && data['info']['status']['msg']) {
						        Parcel.initialize(data['info']['status']['msg'],data['history']);
							    $("#trackResultModal td[data-id='carrier']").html(data['info']['carrier']);
							    $("#trackResultModal td[data-id='awb']").html(data['info']['awb']);
							    $("#trackResultModal td[data-id='status']").html(data['info']['status']['msg']);
							    $("#trackResultModal td[data-id='link']").html(data['info']['link']);
									
							    $("#trackResultModal tbody[data-id='history']").empty();

							    if(data['history'] && data['history'].length > 0) {
								    $(data['history']).each(function(i,o) {
											
									    locationtitle = '';
							            if (o.location) {
									        if (o.location.city) {
										        locationtitle = o.location.city;
										    }
										    if (o.location.country) {
										        if (o.location.city) {
											        locationtitle += ', ';
											    }
														
											    locationtitle += o.location.country;
										    }
									    }
											
									    if (o.activity && o.activity.length > 0) {
									        $(o.activity).each(function(ia,oa) {
										        $("#trackResultModal tbody[data-id='history']").append(
											        '<tr>'
												    +'<th>'+oa.timestamp.date+' '+oa.timestamp.time+'</th>'
												    +'<td>'+oa.status+'</td>'
												    +'<td class="nowrap">'+locationtitle+'</td>'
												    +'</tr>'
											    );
										    });
									    }
								    })
							    }

									
							    $("#trackResultModal").modal('show');
						    } else {
						        $("#alert-contents").html('<p>'+form.data('unknown')+'</p>');
							    $("#alertModal").modal('show');
							    // GETS NO DATA BACK
						    }
					    },
					    complete: function () {
					        form.find('button').removeClass('loading')
						    form.find('button .glyphicon').remove();
					    }
			        });
	            }
	        });
	        });
	    </script>

<?php wp_footer(); ?>

</body>

</html>

