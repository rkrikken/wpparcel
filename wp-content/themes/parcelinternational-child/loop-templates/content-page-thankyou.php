<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

?>

<article class="content" id="post-<?php the_ID(); ?>">

	<div class="entry-content">
	
		<?php get_template_part('partials/content', 'thankyou'); ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
