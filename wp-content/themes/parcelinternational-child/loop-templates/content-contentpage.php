<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<span class="progress-bar"></span>

<?php if( has_post_thumbnail()) { ?>

<section class="storie-content">
	<div class="container-fluid">
		<div class="row d-flex">
			<div class="col-lg-12">
				<div class="content__page-outer">
					<div class="storie-introimg card">
						<?php echo get_the_post_thumbnail( $post->ID, '1700' ); ?> 
					</div>
					<div class="content__page-content">
						<!-- flexibele content -->
						<div class="container">
					        <div class="row">
					        	<div class="col-lg-6 col-sm-12">


									<?php

									// check if the flexible content field has rows of data
									if( have_rows('story_choose_content_header') ):

									     // loop through the rows of data
									    while ( have_rows('story_choose_content_header') ) : the_row();

									        if( get_row_layout() == 'titel' ): ?>

									        		<h1 class="entry-title"><?php the_sub_field('titel'); ?></h1>

									        	<?php

									        elseif( get_row_layout() == 'introductie' ): ?>

									        		<p class="entry-sub"><?php the_sub_field('introductie'); ?></p>
									 
									        	<?php

									         elseif( get_row_layout() == 'hero_btn_link' ): 

												$link = get_sub_field('hero_btn_link');

												if( $link ) { ?>
													
													<div class="btn-outer">
													<a class="btn-02" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
													</div>
													
												<?php }  

									        endif;

									    endwhile;

									else :

									    // no layouts found

									endif;

									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php } ?>

<section class="content__intro" id="post-<?php the_ID(); ?>">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-10">
				<div class="entry-header">

					<!-- flexibele content -->

					<?php

					// check if the flexible content field has rows of data
					if( have_rows('content_introduction') ):

					     // loop through the rows of data
					    while ( have_rows('content_introduction') ) : the_row();

					        if( get_row_layout() == 'introductie' ): ?>

					        	<div class="a-m center">

					        		<h1 class="entry-title"><?php the_sub_field('titel'); ?></h1>
					        		<p class="entry-sub center"><?php the_sub_field('introductie'); ?></p>
					        		<?php
								
									$btn = get_sub_field('btn');

									if( $btn ): ?>
										
										<div class="btn-outer">
										<a class="btn-02" href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>"><?php echo $btn['title']; ?></a>
										</div>

									<?php endif; ?>
					        	</div><!-- end a-m center -->
					        	
					        	<?php

					        endif;

					    endwhile;

					else :

					    // no layouts found

					endif;

					?>

				</div><!-- .entry-header -->
			</div>
		</div>
	</div>
</section><!-- #post-## -->

<!-- end header section -->

</div> <!-- end row -->
</div>	<!-- end container --> 
</wrapper> <!-- end wrapper -->

<div id="storyWrapper"> <!-- start story wrapper -->

<!-- flexibele content -->

<?php

// check if the flexible content field has rows of data
if( have_rows('story_choose_content_body') ):

     // loop through the rows of data
    while ( have_rows('story_choose_content_body') ) : the_row();

        if( get_row_layout() == 'paragraaf' ): ?>

			 <section class="paragraph">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<p><?php the_sub_field('paragraaf'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'quoteparagraaf' ): ?>

			<section class="quoteParagraph">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 d-flex justify-content-center">
							<div class="box">
								<h4 class="stories color"><?php the_sub_field('quote'); ?></h4>
							</div>
						</div>
						<div class="col-lg-12">
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
					</div>
				</div>
			</section>

			<?php

        	elseif( get_row_layout() == 'video' ): ?>

        		<section class="video" style="padding-bottom: 25px;">
        			<div class="container">
        				<div class="row">
        					<div class="col-lg-12">
        						<div class="embed-container d-flex justify-content-center">
									<?php the_sub_field('video'); ?>
								</div>
        					</div>
        				</div>
        			</div>
        		</section>
        	
        	<?php

        	elseif( get_row_layout() == 'cta' ): ?>

			<section class="singlecta">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="singlecta__content">
								<div class="singlecta__title">
									<h3><?php the_sub_field('tekst'); ?></h3>
								</div>

								<?php 

								$link = get_sub_field('cta_link');
								$linktwo = get_sub_field('cta_pagelink');

								if ($link) { ?>

								<div class="singlecta__btn">
									<a class="btn-02" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo the_sub_field('cta_title'); ?></a>

								</div>

								<?php } elseif ($linktwo) { ?>

								<div class="singlecta__btn">
									<a class="btn-02" href="<?php echo $linktwo['url']; ?>" target="<?php echo $linktwo['target']; ?>"><?php echo the_sub_field('cta_title'); ?></a>
								</div>

								<?php } else {} ?>

							</div>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'doubleimg' ): ?>

			<section class="doubleImg">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="img-box card">
								<?php echo wp_get_attachment_image( get_sub_field('imgone'), '1700w',"", ["class" => ""] ); ?>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="img-box card">
							 	<?php echo wp_get_attachment_image( get_sub_field('imgtwo'), '1700w',"", ["class" => ""] ); ?>
							 </div>
						</div>
						<div class="col-lg-12">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        	elseif( get_row_layout() == 'paragraaftitel' ): ?>

			<section class="paragraphTitle">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2><?php the_sub_field('titel'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
					</div>
				</div>
			</section>


<!-- repeater -->


			<?php

        	elseif( get_row_layout() == 'paragraaftitel' ): ?>

			<section class="paragraphTitle">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2><?php the_sub_field('titel'); ?></h2>
							<p><?php the_sub_field('text'); ?></p>
						</div>
					</div>
				</div>
			</section>


			<?php

        	elseif( get_row_layout() == 'singlecta' ): 

        		 $pagelink = get_sub_field('pagelink'); 
        		 $link = get_sub_field('link'); 
        		 $title = get_sub_field('singlectatitle');
        		 $classname = get_sub_field('classname');

        		 if($pagelink) { ?>

				<section class="singlecta">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="singlecta-outer">
									<a class="singlectabtn <?php echo $classname; ?>" href="<?php echo $pagelink ?>" target="_blank"><?php echo $title ?></a>
								</div>
							</div>
						</div>
					</div>
				</section>

				<?php } else { ?>
					<section class="singlecta">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="singlecta-outer">
										<a class="singlectabtn <?php echo $classname; ?>" href="<?php echo $link ?>" target="_blank"><?php echo $title ?></a>
									</div>
								</div>
							</div>
						</div>
					</section>
				<?php } ?>




		<?php // check current row layout

        elseif( get_row_layout() == 'columncontent' ):

        	// check if the nested repeater field has rows of data
        	if( have_rows('repeater') ): ?>

			 	
			<section class="column">
			 	<div class="container">
			 		<div class="row full">

			 	
	<?php
			 	// loop through the rows of data
			    while ( have_rows('repeater') ) : the_row();

					$column = get_sub_field('column');

					echo '<div class="col-lg col-sm-12">';

					echo $column;

					echo '</div>';

				endwhile; ?>

				
					
					</div>
				</div>
			</section>
				

			<?php endif;

			elseif( get_row_layout() == 'columnbox' ):

        	// check if the nested repeater field has rows of data
        	if( have_rows('repeater') ): ?>

			 	
			<section class="columnbox">
			 	<div class="container">
			 		<div class="row full d-flex align-items-stretch">
			 			<div class="owl-carousel owl-theme" id="homeSlider">

			 	
	<?php
			 	// loop through the rows of data
			    while ( have_rows('repeater') ) : the_row();

			    	$image = wp_get_attachment_image( get_sub_field('image'), '900w',"", ["class" => ""] );
			    	$titel = get_sub_field('titel');
					$column = get_sub_field('column');
					$link = get_sub_field('pagelink');

					if ($link) {

					echo '<div class="item col-lg col-sm-12">';  ?>
				
						<a class="link__content" href="<?php echo $link; ?>">

							<?php if( $image ) { ?>
								<div class="image-outer">
									<?php echo $image; ?>
								</div>

							<?php } ?>
								
									<h4><?php echo $titel; ?></h4>
									<?php echo $column; ?>	
								
						</a>
					
				   <?php  echo '</div>';

					} else { 

						echo '<div class="item col-lg col-sm-12">';  ?>

								<div class="link__content" href="<?php echo $link; ?>">

									<?php if( $image ) { ?>
										<div class="image-outer">
											<?php echo $image; ?>
										</div>
									<?php } ?>

									<div class="content">
										<h4><?php echo $titel; ?></h4>
										<?php echo $column; ?>	
									</div>
								</div>

						 <?php  echo '</div>';

					}

				endwhile; ?>

				
						</div>
					</div>
				</div>
			</section> 

			<?php endif;

		elseif( get_row_layout() == 'imagebox' ):

        	// check if the nested repeater field has rows of data
        	if( have_rows('repeater') ): ?>

			 	
			<section class="imagebox">
			 	<div class="container">
			 		<div class="row full d-flex">
			 			<div class="col-lg-12">
			 				<div class="owl-carousel owl-theme" id="homeSlider">

			 	
	<?php
			 	// loop through the rows of data
			    while ( have_rows('repeater') ) : the_row();

			    	$number = get_sub_field('number');
			    	$titel = get_sub_field('titel');
					$column = get_sub_field('column');
					$link = get_sub_field('pagelink');

					echo '<div class="item no-gutters col-lg col-sm-12">';  ?>
				
					<a class="image__link" href="<?php echo $link; ?>">
						<?php echo wp_get_attachment_image( get_sub_field('image'), '1700w',"", ["class" => ""] ); ?>
					
						<div class="img-content-box">
								<h3><?php echo $titel; ?></h3>
								<p><?php echo $column; ?></p>
						</div>

						<?php if ($number) { ?>
							<span class="number"><?php echo $number; ?></span>
						<?php } ?>
					</a>
					
				   <?php  echo '</div>';


				endwhile; ?>

							</div>
						</div>
					</div>
				</div>
			</section> 

			<?php endif;
        	
        	elseif( get_row_layout() == 'singleimage' ): ?>

			<section class="singleImg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="img-box card">
								 <?php echo wp_get_attachment_image( get_sub_field('imgone'), '1700w',"", ["class" => ""] ); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
        	
        	<?php

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>

<?php if ( is_single() ) { ?>

	<?php if( the_tags() ){ ?>

	<section class="tagcloud">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="tags">
						 <?php the_tags( '<ul><li class="tag">', '</li><li class="tag">', '</li></ul>' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php } ?>

	</div> <!-- end story wrapper -->

	<section class="postedBy">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-12">
					<div class="postedBy__head">
						<div class="author">
							<?php  
							    $user_id = 1;
							    echo get_avatar($user_id, 60); 
							?>
							<div class="author__description">
								<h4><?php the_field('Main_title', option); ?></h4>

								<p><?php the_field('Main_sub', option); ?></p>
							</div>
						</div>

						<div class="author__content">
							<p><?php the_field('Main_description', option); ?></p>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part('partials/content', 'morecontent'); ?>

<?php } ?>

</div> <!-- end story wrapper -->

<div class="row"> <!-- start row -->