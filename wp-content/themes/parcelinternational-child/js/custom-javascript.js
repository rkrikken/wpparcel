var $ = jQuery.noConflict();

$(document).ready(function() {
    
      $(window).scroll(function () {
          //if you hard code, then use console
          //.log to determine when you want the 
          //nav bar to stick.  
          console.log($(window).scrollTop())
        if ($(window).scrollTop() > 24) {
          $('#nav_bar').addClass('navbar-fixed');
          $('.topnav__container').css('background-color', 'white');
        }
        if ($(window).scrollTop() < 25) {
          $('#nav_bar').removeClass('navbar-fixed');
          $('.topnav__container').css('background-color', 'transparent');
        }
      });

    var logo = document.createElement("img");
    logo.setAttribute("id", "imgCheck");
    logo.setAttribute("src", "http://parcelinternational.com/wp-content/uploads/2018/11/cropped-logo_slogan_parcelinternational.png");
    logo.setAttribute("width", "100");

    var bar = document.querySelector(".moove-gdpr-info-bar-content p");

    $(logo).insertBefore(bar);

    $('#moove_gdpr_cookie_info_bar').css({
        'min-height' : '100vh',
        'display' : 'flex',
        'flex-direction' : 'column',
        'justify-content' : 'center',
        'aling-items' : 'center',
        'background-color' : 'rgba(0,0,0,0.5)'
    });

    $('.moove-gdpr-info-bar-content').css({
        'display' : 'flex',
        'flex-direction' : 'column',
        'justify-content' : 'center',
        'aling-items' : 'center',
        'text-align' : 'center',
        'background-color' : 'white',
        'border-radius' : '5px',
        'border' : '1px solid rgba(0,0,0,0.1)',
        'border-bottom' : '2px solid rgba(0,0,0,0.25)',
        'width' : 'auto',
        'max-width' : '350px',
        'margin' : '0 auto',
        'padding' : '15px'
    });

    $('.moove-gdpr-info-bar-content p').css({
        'font-size' : '16px',
        'line-height' : '22px',
        'font-family' : 'Roboto',
        'color' : '#202020',
        'font-weight' : '300'
    });
    
    $('.moove-gdpr-cookie-notice').css({
        'margin-bottom' : '25px'
    });

    $('.moove-gdpr-info-bar-content .moove-gdpr-button-holder').css('padding', '0');

    $('.moove-gdpr-modal-right-content').css("background-color", "white");
    $('.moove-gdpr-modal-footer-content').css("background-color", "white");

    $('.lity').css({
        'background' : 'none!important',
        'background-color' : 'rgba(0,0,0,0.25)'
    });


    // get modals 
    $('li.register-header a').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('li.register a').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('.register').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('a.register').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('#register-footer').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('li.tracktrace a').attr('data-target', '#tracktraceModal').attr('data-toggle', 'modal'); // get modal-->
    $('.register').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
    $('.offertelink').attr('data-target', '#pi-login').attr('data-toggle', 'modal'); // login-->
    $('.sendBtnlight').attr('data-target', '#registerModal').attr('data-toggle', 'modal'); // get modal-->
  
    $(".hamburger").click(function(e) {
        e.preventDefault();
        $("body").toggleClass("flow");
    });

    $('.wpml-ls-native').eq(0).hide();
    
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

   // make the entire area clickable
    $('.overview').on('click', function(e) {
      window.location = $(this).find('a').attr('href');
    });
    $('.overview').on('click', function(e) {
      window.location = $(this).find('a').attr('href');
    }).on('hover', function(e) {
      $(this).css('cursor', 'pointer').css('cursor', 'hand');
    });

  $('#testimonialSlider').owlCarousel({
    dots:true,
    loop:true,
    margin:10,
    nav:true,
    navText: ["<i class='fa fa-angle-left' style='font-size: 32px; left: 100px;'></i>", "<i class='fa fa-angle-right' style='font-size: 32px; right: 100px;'></i>"],
    responsive:{
        0:{
            items:1,
             nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1
        }
    }
});

  $('#uspSlider').owlCarousel({
    dots:true,
    nav:false,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    navText: ["<i class='fa fa-angle-left' style='font-size: 32px; left: 100px;'></i>", "<i class='fa fa-angle-right' style='font-size: 32px; right: 100px;'></i>"],
    responsive:{
        0:{
            items:1,
             nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:false
        }
    }
});

    $('#homeSlider').owlCarousel({
    dots:true,
    nav:false,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    navText: ["<i class='fa fa-angle-left' style='font-size: 32px; left: 100px;'></i>", "<i class='fa fa-angle-right' style='font-size: 32px; right: 100px;'></i>"],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1,
            loop:true
        },
        1000:{
            items:3,
            loop: false
        }
    }
});

  $('.effectup').on('inview', function(event, isInView){
    if(isInView){
        // $(this).removeClass('animated fadeOut');
        $(this).addClass('animated fadeInUp');
    } else {
        // $(this).removeClass('animated fadeInUp');
        // // $(this).addClass('animated fadeOut');
    }
});

$('.effectin').on('inview', function(event, isInView){
    if(isInView){
        // $(this).removeClass('animated fadeOut');
        $(this).addClass('animated fadeIn').delay(1000);
    } else {
        // $(this).removeClass('animated fadeIn');
        // // $(this).addClass('animated fadeOut');
    }
});

$('.effectdown').on('inview', function(event, isInView){
    if(isInView){
        // $(this).removeClass('animated fadeOut');
        $(this).addClass('animated fadeInDown');
    } else {
        // $(this).removeClass('animated fadeIn');
        // // $(this).addClass('animated fadeOut');
    }
});

// add functionality to medium and large box

$("#small-box").hide();
$("#titlesmall").hide();

$( "#small" ).click(function() {
  $(this).addClass('active');
  $('#large').removeClass('active');
  $( "#small-box" ).fadeIn('slow').show();
  $( "#large-box" ).fadeOut('slow').hide();
  $( "#titlesmall" ).fadeIn('slow').show();
  $( "#titlelarge" ).fadeOut('slow').hide();
});

$( "#large" ).click(function() {
  $(this).addClass('active');
  $('#small').removeClass('active');
  $( "#small-box" ).fadeOut('slow').hide();
  $( "#large-box" ).fadeIn('slow').show();
  $( "#titlelarge" ).fadeIn('slow').show();
  $( "#titlesmall" ).fadeOut('slow').hide();
});

// scroll

$(".scroll").click(function() {
    var offset = 20; //Offset of 20px

    $('html, body').animate({
        scrollTop: $("#scrollone").offset().top + offset
    }, 2000);
});

$(window).scroll(function(){
    $(".fadeout").css("opacity", 1 - $(window).scrollTop() / 250);
  });

// menu scroll
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header.one').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header.one').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header.one').removeClass('nav-up').addClass('nav-down');
        }
    }
    
    lastScrollTop = st;
}

// scroll 

 var getMax = function(){
        return $(document).height() - $(window).height();
    }
    
    var getValue = function(){
        return $(window).scrollTop();
    }
    
    if('max' in document.createElement('progress')){
        // Browser supports progress element
        var progressBar = $('progress');
        
        // Set the Max attr for the first time
        progressBar.attr({ max: getMax() });

        $(document).on('scroll', function(){
            // On scroll only Value attr needs to be calculated
            progressBar.attr({ value: getValue() });
        });
      
        $(window).resize(function(){
            // On resize, both Max/Value attr needs to be calculated
            progressBar.attr({ max: getMax(), value: getValue() });
        });   
    }
    else {
        var progressBar = $('.progress-bar'), 
            max = getMax(), 
            value, width;
        
        var getWidth = function(){
            // Calculate width in percentage
            value = getValue();            
            width = (value/max) * 100;
            width = width + '%';
            return width;
        }
        
        var setWidth = function(){
            progressBar.css({ width: getWidth() });
        }
        
        $(document).on('scroll', setWidth);
        $(window).on('resize', function(){
            // Need to reset the Max attr
            max = getMax();
            setWidth();
        });
    }

});
