<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
	
$i = ICL_LANGUAGE_CODE;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- start Datalayer --> 
	<script>

		window.dataLayer = [{
			'page':{
		        'type':'',
				'language': '<?php echo $i; ?>',	
			}
		}]

	</script>
	<!-- end Datalayer -->
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PMF84S3');</script>
	<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link href="https://fonts.googleapis.com/css?family=Exo:300,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="https://ship.parcelinternational.com/packs/website-42820b52bc4f28b1d9ecbe624f0a5ae9.css">
	<link rel="stylesheet" type="text/css" href="http://assets1.chat.freshdesk.com/css/visitor.css">

	<script type="text/javascript" src="https://ship.parcelinternational.com/packs/website-e8d4e1f3b69ea51592d1.js" defer></script>
	<script src="http://chat.freshdesk.com:80/visitors/grantPermissions?callback=Pusher.auth_callbacks%5B'2'%5D&amp;socket_id=237057.8723946&amp;channel_name=private-visitors-dbb55753fa8fe010d9d8cbadf8d5f6ad-online-visitor1348397217786&amp;userId=visitor1348397217786&amp;siteId=dbb55753fa8fe010d9d8cbadf8d5f6ad" defer></script>
	<script src="http://chat.freshdesk.com:80/visitors/grantPermissions?callback=Pusher.auth_callbacks%5B'1'%5D&amp;socket_id=237057.8723946&amp;channel_name=private-dbb55753fa8fe010d9d8cbadf8d5f6ad-visitor1348397217786&amp;userId=visitor1348397217786&amp;siteId=dbb55753fa8fe010d9d8cbadf8d5f6ad" defer></script>
	<script>window.shipOrigin = "https://ship.parcelinternational.com";</script>

	<style type="text/css">#lc_chat_layout {right:5px} </style>
	<style type="text/css">  #lc_chat_layout #lc_chat_header {color:#FFFFFF;background-color:#1895c8}  /* #lc_chat_layout #lc_chat_container  {border-color: rgba(24,149,200,0.3)} */ </style>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<svg class="c-layout__triangle" viewBox="0 0 569 466" preserveAspectRatio="none">
<path d="M0 422L569 62V0H0z"></path>
</svg>


<div id="wrapper">

<div class="hfeed site" id="page">

	<header class="one nav-down">
	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<div class="container-fluid topnav__container">
			<div class="container">
				<div class="row topnav__row">
				
					<?php
					wp_nav_menu( array( 
					    'theme_location' => 'topmenu', 
					    'container_class' => 'topmenu' ) ); 
					?>
					
				</div>
			</div>
		</div>

		<nav class="navbar navbar-expand-xl" id="nav_bar">

		<?php if ( 'container' == $container ) : ?>
			<div class="container" >
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle navigation">

<style>
.containerMenu {
    display: inline-block;
    cursor: pointer;
}

.bar1, .bar2, .bar3 {
    width: 28px;
    height: 3px;
    background-color: #10425E;
    margin: 6px 0;
    transition: 0.4s;
}

.changehamburger .bar1 {
    -webkit-transform: rotate(-45deg) translate(-6px, 3px);
    transform: rotate(-45deg) translate(-6px, 3px);
}

.changehamburger .bar2 {opacity: 0;}

.changehamburger .bar3 {
    -webkit-transform: rotate(45deg) translate(-8px, -8px);
    transform: rotate(45deg) translate(-8px, -8px);
}
</style>

<div class="container hamburger" id="menu-toggle" onclick="myFunction(this)">
  <div class="bar1"></div>
  <div class="bar2"></div>
  <div class="bar3"></div>
</div>

<script>
	 function myFunction(x) {
        x.classList.toggle("changehamburger");
    }
</script>

				</button>
				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'd-none d-xl-block',
						'container_id'    => 'desktop-menu',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
	</header>

	<div class="mobilenav">
		<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'd-xl-none',
						'container_id'    => 'mobile-menu',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>
	</div>

<div id="page-content-wrapper">