<div class="pi-svg">

		<svg width="1440px" id="pi-svg" viewBox="0 0 1440 242" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		    <!-- Generator: Sketch 51.1 (57501) - http://www.bohemiancoding.com/sketch -->
		    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" opacity="0.5">
		        <g id="INLOG" transform="translate(0.000000, -64.000000)" fill="#10425E">
		            <g id="SVG-image" transform="translate(-120.000000, 64.000000)">
		                <g id="vectors">
		                    <g>
		                        <polygon id="Triangle" opacity="0.1" transform="translate(948.057617, 121.000000) scale(-1, 1) translate(-948.057617, -121.000000) " points="831.492188 0 1094.11523 1 802 242"></polygon>
		                        <polygon id="Triangle" opacity="0.2" points="291.257813 238.222656 262.136719 0.21484375 508.762207 186.214355"></polygon>
		                        <polygon id="Triangle" opacity="0.1" transform="translate(145.566895, 119.612793) scale(-1, 1) translate(-145.566895, -119.612793) " points="28.6650391 1 291.133789 1 0 238.225586"></polygon>
		                        <polygon id="Triangle" opacity="0.15" points="615.53125 1 878 1 508.681641 186.417969"></polygon>
		                        <polygon id="Triangle" opacity="0.1" points="1417.53125 1 1680 1 1308.59766 190"></polygon>
		                        <polygon id="Triangle" opacity="0.25" points="1093.25781 241.222656 1064.08008 0 1311.48438 190.039063"></polygon>
		                    </g>
		                </g>
		            </g>
		        </g>
		    </g>
		</svg>

</div>