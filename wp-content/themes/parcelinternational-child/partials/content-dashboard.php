<section class="mijnzendingen">

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="mijnzendingen__header">
					<h1 class="heading-h1">Mijn zendingen</h1>
				</div>
			</div>
		</div>
	</div>
		
	<div class="mijnzendingen__body">
		<div class="container">
		<div class="row d-flex align-items-stretch">
			<div class="col-lg-4 d-flex align-items-stretch">
				<div class="mijnzendingen__body-item pi-card">
					<div class="item-img">
						<img src="http://localhost/releases/wp-content/uploads/2018/10/2000px-TNT_Express_Logo.png" style="width: 150px;">
					</div>
					<a href="http://localhost:3000/releases/index.php/dashboard/mijn-zending-details/" class="card-link">
						Express zending
					</a>
				</div>
			</div>
			<div class="col-lg-4 d-flex align-items-stretch">
				<div class="mijnzendingen__body-item pi-card-transparent">
					<p class="item-content">
						Vergelijk en <a style="color: #1D95C5;" href="#">boek uw volgende zending</a>
					</p>
				</div>
			</div>
		</div>
		</div>
	</div>

		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="mijnzendingen__link">
					<a href="http://localhost:3000/releases/index.php/dashboard/mijn-zendingen-overzicht/">Toon alle zendingen</a>
				</div>
			</div>
		</div>
		</div>

</section>

<section class="quicklinks">

		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="quicklinks__header">
						<h1 class="heading-h1">Quick Links</h1>
					</div>
				</div>
			</div>
		</div>

	<div class="quicklinks__body">
		<div class="container">
		<div class="row d-flex align-items-stretch">
			<div class="col-lg-4 d-flex align-items-stretch">
				<div class="quicklinks__body-item pi-card">
					<div class="item-title">
						Link Title
					</div>
					<a class="card-link">
						Bekijk optie
					</a>
				</div>
			</div>
			<div class="col-lg-4 d-flex align-items-stretch">
				<div class="quicklinks__body-item pi-card">
					<div class="item-title">
						Link Title
					</div>
					<a class="card-link">
						Bekijk optie
					</a>
				</div>
			</div>
			<div class="col-lg-4 d-flex align-items-stretch">
				<div class="quicklinks__body-item pi-card">
					<div class="item-title">
						Link Title
					</div>
					<a class="card-link">
						Bekijk optie
					</a>
				</div>
			</div>
		</div>
		</div>
	</div>

		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="quicklinks__link">
					<a href="">Quick links instellen</a>
				</div>
			</div>
		</div>
		</div>

</section>