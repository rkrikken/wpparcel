<section class="question">
	
	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-8">
				<div class="question__body">
					<div class="question__body-content">
						<h2><?php the_title(); ?></h2>
						<p><?php the_content(); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php get_template_part('partials/content', 'morecontent'); ?>


