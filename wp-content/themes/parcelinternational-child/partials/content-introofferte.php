<section class="introduction__offerte">
	<div class="hide">
		<?php get_template_part('partials/content', 'svg'); ?>
	</div>
		<div class="row d-flex justify-content-center">
			<div class="col-lg-6">
				<div class="introduction__content center fadeout">
					<h1 id="titlesmall"><?php the_field('introtitel_sec'); ?></h1>
					<h1 id="titlelarge"><?php the_field('introtitel_primary'); ?></h1>
					<p><?php the_field('introsub'); ?></p>
					<div class="btn__outer">
						<button id="small"><?php the_field('introbuttontitel_sec'); ?></button>
						<button class="active" id="large"><?php the_field('introbuttontitel'); ?></button>
					</div>
				</div>
			</div>
		</div>
</section>

<section class="choose">
	<div class="hide">
		<?php get_template_part('partials/content', 'svg'); ?>
	</div>
	<div class="container">
		<div class="row d-flex justify-content-center">
			
			<div class="col-lg-6">
				<div class="choose__card card effectup">
				<div class="row change d-flex align-items-stretch" style="height: 100%;">
					<div class="col-lg-12" id="small-box" style="min-height: 100%;">
						<div class="choose center">
							<h2><?php the_field('cardtitel_sec'); ?></h2>

						<?php 

						$btnone = get_field('card_button_tekst_one_sec'); 
						$cardsub = get_field('cardsub_sec');

						?>

						<?php if($cardsub) { ?>

							<p><?php the_field('cardsub_sec'); ?></p>

						<?php } ?>

						<?php if($btnone) { ?>
							<div class="btn__outer">
								<a class="sendBtnlight">
									<?php the_field('card_button_tekst_one_sec'); ?>
								</a>
							</div>

						<?php } ?>
							<p class="second"><?php the_field('description_sec'); ?></p>


							<div class="outer__usp">
								<?php if( have_rows('card_usp_repeater_sec') ): ?>

									<ul class="usp">

									<?php while( have_rows('card_usp_repeater_sec') ): the_row(); 

										// vars
										$usptitel = get_sub_field('usptitel');

										?>

										<li>

											<?php echo $usptitel; ?>

										</li>

									<?php endwhile; ?>

									</ul>

								<?php endif; ?>
							</div>
						</div>

						<a class="footer__cta scroll footer__offerte">
							 	<?php the_field('button_titel_two_sec'); ?>
						</a>
						
					</div>
					<div class="col-lg-12" id="large-box">
						
						<div class="choose center">
							<h2><?php the_field('cardtitel'); ?></h2>

						<?php 

						$btntwoprim = get_field('card_button_tekst_one'); 
						$cardsubprim = get_field('cardsub');

						?>

						<?php if($cardsub) { ?>

							<p><?php echo $cardsub; ?></p>

						<?php } ?>

						<?php if($btntwo) { ?>
							<div class="btn__outer">
								<a class="sendBtnlight">
									<?php echo $btntwo; ?>
								</a>
							</div>

						<?php } ?>

							<p><?php the_field('cardsub'); ?></p>
							<div class="btn__outer">
								<a class="sendBtn scroll">
									<?php the_field('card_button_tekst_one'); ?>
								</a>
							</div>
							<p class="second"><?php the_field('description'); ?></p>

							<div class="outer__usp">


								<?php if( have_rows('card_usp_repeater') ): ?>

									<ul class="usp">

									<?php while( have_rows('card_usp_repeater') ): the_row(); 

										// vars
									
										$usptitel = get_sub_field('usptitel');

										?>

										<li>

											<?php echo $usptitel; ?>

										</li>

									<?php endwhile; ?>

									</ul>

								<?php endif; ?>

							</div>

							</div>
							
							<a class="footer__cta scroll footer__offerte">
							 	<?php the_field('button_titel_two'); ?>
							</a>
							
						</div>
						
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="cta__contact" id="scrollone">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 d-flex justify-content-center align-items-center">
				<div class="whoarewe">
					<h2><?php the_field('titel'); ?></h2>
					<p>
					<?php the_field('tekst'); ?>
					</p>

					<quote>
						<?php the_field('quotetekst'); ?>
						<span>
							<div class="left">


								<?php 

								$image = get_field('image');
								$size = 'full'; // (thumbnail, medium, large, full or custom size)

								if( $image ) {

									echo wp_get_attachment_image( $image, $size );

								}

								?>
								
							</div>
							<div class="right">
								<strong><?php the_field('naam'); ?></strong> 
								</br> <?php the_field('functie'); ?>
							</div>
						</span>
					</quote>
				</div>

			</div>
			<div class="col-lg-6">
				<div id="offerte__form">
					<h2><?php the_field('contactfield_titel'); ?></h2>
				<?php the_field('contactfield'); ?>
				</div>
			</div>
</section>