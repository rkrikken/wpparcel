<section class="moreContent">
	<div class="container">
		<div class="row">

			<?php
			global $post;
			$args = array( 
				'post_type' => post,
				'posts_per_page' => 3, 
				'order'=> 'RAND', 
				'orderby' => 'title',
				'post__not_in' => array($id)
				);
			$postslist = get_posts( $args );
			foreach ( $postslist as $post ) :
			  setup_postdata( $post ); ?> 
			  <div class="col-lg-4">
				<div class="item article-div card">
					<a class="article-link" href="<?php the_permalink(); ?>"><h3 class="stories"><?php the_title(); ?></h3></a>
					<div class="article-img">
					<?php the_post_thumbnail(); ?>
					</div>
				</div>
			</div>
			<?php
			endforeach; 
			wp_reset_postdata();
			?>

		</div>
	</div>
</section>