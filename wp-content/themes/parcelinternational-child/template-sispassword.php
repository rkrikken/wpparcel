<?php

/* Template Name: SIS password */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<?php if(is_page_template( 'template-sispassword.php' )) : ?>

	<style>
	.navbar-expand-xl{
		background-color: #ffffff!important;
	}

	.topnav__container {
		background-color: #ffffff!important;
	}
	</style>

<?php endif; ?>

<?php get_template_part( 'loop-templates/content', 'page-sispassword' ); ?>

<?php get_footer(); ?>