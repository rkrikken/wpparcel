<script>
var $ = jQuery.noConflict();

$(document).ready(function() {

var getLangCode = '<?php echo apply_filters( 'wpml_current_language', NULL );  ?>';

	window.dataLayer = [{
		'page':{
	        'type':'',		// mag voor nu leeg blijven
		'language': getLangCode,	// Waarde dynamisch vullen met: NL, EN, DE of CN
	    }
	}]

});
</script>

