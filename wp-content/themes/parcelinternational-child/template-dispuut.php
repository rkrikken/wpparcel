<?php

/* Template Name: Template Dispuut */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<?php if(is_page_template( 'template-dispuut.php' )) : ?>

	<style>
	.navbar-expand-xl{
		background-color: #ffffff!important;
	}

	.topnav__container {
		background-color: #ffffff!important;
	}
	</style>

<?php endif; ?>

<?php get_template_part( 'loop-templates/content', 'page-dispuut' ); ?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->


<?php get_footer(); ?>