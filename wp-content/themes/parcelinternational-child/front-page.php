<?php

/* Template Name: Home Template */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<?php if ( is_front_page()) { ?>

	<style>
		.wrapper#page-wrapper { 
			padding: 0px;
		}

		#page-content-wrapper {
			padding: 0px;
		}
	</style>

<?php } ?>

<section class="hero__homepage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 no-gutters">
				<div class="hero__outer">
					<div class="hero-image">
					<?php echo wp_get_attachment_image( get_field('hero_img'), '1700w',"", ["class" => "hero__homepage-img"] ); ?>
					</div>
					<div class="container hero__homepage-inner">
						<div class="row">
							<div class="col-lg-7 col-sm-12">
						
							<div class="hero__content">
								<h1 class="white"><?php the_field('hero_title');?></h1>
								<p class="white"><?php the_field('hero_sub');?></p>
							</div>

							<div class="hero__cta">
								<div class="hero__cta-top">
									<a class="hero__box-content register">
										<h4><?php the_field('cta_box_title'); ?></h4>
									</a>
								</div>
								<div class="hero__cta-bottom">
									<ul class="usp">
										<li><a class="register d-none d-sm-block" href=""><?php the_field('cta_one'); ?></a></li>
										<li><a class="register d-none d-sm-block" href=""><?php the_field('cta_two'); ?></a></li>
										<li><a class="register d-none d-sm-block" href=""><?php the_field('cta_three'); ?></a></li>
									</ul>
									<div class="btn-outer hero iconafter">
										<?php 

										$link = get_field('hero_pagelink');

										if( $link ): ?>
											
											<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

										<?php endif; ?>
									</div>
								</div>
							</div>

							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</section>

<section class="hero__usp">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="hero__usp-content">
					<?php if( have_rows('usp_repeater') ): ?>
						<ul>
						<?php while( have_rows('usp_repeater') ): the_row(); 
							// vars
							$image = get_sub_field('image');
							$usp = get_sub_field('usp');
							?>
							<li>
								<img class="small" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								<?php echo $usp; ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<?php if( have_rows('status_repeater') ): ?>
						<ul>
						<?php while( have_rows('status_repeater') ): the_row(); 
							// vars
							$logo = get_sub_field('logo');
							?>
							<li>
								<img class="big" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt'] ?>" />
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>	
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home__prop">
	<div class="container">
		<div class="row d-flex justify-content-center center">
			<div class="col-lg-8">
				<div class="home__prop-content">
					<h2><?php the_field('prop_title'); ?></h2>
					<p><?php the_field('prop_sub'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="home__split">
	<div class="container">
		<div class="row d-flex justify-content-between">
			
			<?php if( have_rows('split') ): ?>

				<div class="col-lg-6 col-sm-12">

				<?php while( have_rows('split') ): the_row(); 

					// vars
					$image = get_sub_field('prop_image');
					$title = get_sub_field('prop_title');
					$sub = get_sub_field('prop_sub');
					$link = get_sub_field('prop_link');

					?>

					
						<div class="row">
							<div class="prop-section">
								<div class="col">

									<div class="home__split-img">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>

								</div>
								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
									<div class="home__split-content">
										<h3><?php echo $title; ?></h3>
										<p><?php echo $sub; ?></p>
										<a class="button iconafter" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
									</div>
								</div>
							</div>
						</div>
					

				<?php endwhile; ?>

				</div>

			<?php endif; ?>

				<div class="col-lg-6">
					<div class="home__video">
						<?php the_field('home_video'); ?>
					</div>
				</div>
			
		</div>
	</div>
</section>

<div class="wrapper" id="page-wrapper">
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<div class="row">
			<main class="site-main" id="main">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'loop-templates/content', 'contentpage' ); ?>
					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>
				<?php endwhile; // end of the loop. ?>
			</main><!-- #main -->
		</div><!-- .row -->
	</div><!-- Container end -->
</div><!-- Wrapper end -->

<?php get_footer(); ?>
