<?php

/* Template Name: url list */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper dashboard" id="page-wrapper">

	<?php get_template_part('partials/content', 'pisvg'); ?>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-lg-12">

				<?php 
				/*
Template Name: List All Post
*/
 
// get all languages
$languages = icl_get_languages( 'skip_missing=0' );
 
 
foreach( (array) $languages as $lang ) {
 
    // change language
    do_action( 'wpml_switch_language', $lang['code'] ); 
     
    // query
    $posts = new WP_Query( array(
        'post_type' => 'any',
        'posts_per_page' => -1,
        'post_status' => 'any',
        'suppress_filters' => false
    ) );
 
    $posts = $posts->posts;
 
    echo "<h2>{$lang['code']}</h2>";
    foreach( (array) $posts as $post ) {
        echo get_permalink( $post->ID ) . "<br />";
    }
}
?>
			</div>

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->


<?php get_footer(); ?>